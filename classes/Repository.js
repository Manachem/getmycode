const User = require("./User.js")
const Room = require("./Room.js")
const Connection = require("./Connection.js")
const Writer = require("./Writer.js")

class Repository {

  constructor() {
    this.users = [];
    this.rooms = [];
    this.connections = [];
    this.writers = []; 
  }

  getUsersInRoom(roomId){
  	var users = [];
   	this.connections.forEach(function(connection){
   		if( connection.room.id == roomId && users.indexOf(connection.user) == -1){
			  users.push(connection.user);
   		}
   	});
    return users;
  }

  displayUserConnected(roomId){
    var t = this;
    var users = [];
    var writers = [];
    var idUsers = [];
    this.connections.forEach(function(connection){
      if( connection.room.id == roomId && idUsers.indexOf(connection.user.id) == -1){
        if(t.userIsWriter(roomId,connection.user.id)){
            idUsers.push(connection.user.id);
            writers.push({'id':connection.user.id , 'username':connection.user.username });
        } else {
            users.push({'id':connection.user.id , 'username':connection.user.username });
            idUsers.push(connection.user.id); 
        }
      }
    });

    return {'users' : users, 'writers':writers};
  }

  userIsWriter(roomId,userId){
    var result = false;
    this.writers.forEach(function(e){
      if(e.roomId == roomId && e.userId == userId){
        result = true;
      }
    });
    return result;
  }

  getUserRoom(userId){
    var rooms = [];

    this.connections.forEach(function(connection){
      if( connection.user.id == userId ){
        rooms.push(connection.room);
      }
    });

    return rooms;
  }

  getUser(userId){
  	var result = null;
   	this.users.forEach(function(user){
   		if( user.id == userId ){
			   result = user;
   		}
   	});
	  return result;
  }

  getUserByFingerPrint(fingerPrint){
    var result = null;
    this.users.forEach(function(user){
      if( user.fingerPrint == fingerPrint ){
         result = user;
      }
    });
    return result;
  }

  deleteConnection(socketId){
     var connections = [];
     var result = null;
     this.connections.forEach( function(connection){
         if(connection.socket != socketId){
            connections.push(connection);
         } else {
           result = connection;
         }
     });
     this.connections = connections;
     return result;
  }

  deleteUser(userId){
  	var newUsers = [];
   	this.users.forEach(function(user){
   		if( userId != user.id ){
			  newUsers.push(user);
   		}
   	});
	  this.users = newUsers;
  }

  getRoom(idRoom){
    var result = null;
    this.rooms.forEach(function(room){
      if( room.id == idRoom ){
         result = room;
      }
    });
    return result;
  }

  getNewIdRoom(){
    var status = true
    var idRoom = Math.round((Math.pow(36, 3 + 1) - Math.random() * Math.pow(36, 3))).toString(36).slice(1);
    this.rooms.forEach(function(room){
      if(room.id == idRoom){
        status = false; 
      }  
    });
    return status ? idRoom : getNewIdRoom();
  }

  checkRoomExist(idRoom){
    var status = false
    this.rooms.forEach(function(room){
      if(room.id == idRoom){
        status = true; 
      }  
    });
    return status;
  }

  createNewRoom(roomId){
     var room = new Room(roomId);
     this.rooms.push(room);
     return room;
  }

  addUserInRoom(FingerPrint, socketId, writer ,room){
  
    var user = this.getUserByFingerPrint(FingerPrint);

    if( user == null){
      console.log('création utilisateur');

      user = new User(FingerPrint);
      var connection = new Connection(room,user, socketId, writer);
      this.connections.push(connection);
      this.users.push(user);
    } else {

      console.log('Récupération utilisateur');
      var connection = new Connection(room, user, socketId, writer);
      this.connections.push(connection);
    }

    if (writer){
       this.writers.push(new Writer(user.id,room.id));
    }

    return user;
  }

}

module.exports = Repository