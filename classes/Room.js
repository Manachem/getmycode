class Room {

  constructor(idRoom) {
    this.id = idRoom;
    this.content = "";
  }

  setContent(content) {
  	this.content = content;
  }

  getContent(content) {
  	return this.content;
  }

}

module.exports = Room