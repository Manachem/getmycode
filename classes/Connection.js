class Connection {

  constructor(room, user , socketId, writer) {
    this.room = room;
    this.user = user;
    this.socket = socketId;
    this.writer = writer;
  }

}

module.exports = Connection