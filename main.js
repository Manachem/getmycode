var http = require('http');
var express = require('express');
var app = express();

app.use('/assets', express.static(__dirname + '/assets'));
var server = http.createServer(app);
var io = require('socket.io').listen(server);
const Repository = require("./classes/Repository.js")
var repo = new Repository();

server.listen(8080);

// Accueil qui redirige vers une room
app.get('/', function (req, res) {
	var idRoom = repo.getNewIdRoom();
    res.redirect('/'+idRoom)
});

app.get('/:idRoom', function (req, res) {
    res.render('index.ejs');
});

// Socket IO
io.sockets.on('connection', function (socket) {

	socket.on('joinRoom', function (res) {
		if(repo.checkRoomExist(res.idRoom)) {
			//Join room
			socket.join(res.idRoom);
			var room = repo.getRoom(res.idRoom);
			var user = repo.addUserInRoom(res.fingerPrint, socket.id, false ,room )
    		socket.emit('getCode', room.getContent());
    		io.to(res.idRoom).emit('userJoinRoom', repo.displayUserConnected(res.idRoom));
		} else {
			//Create room
			socket.join(res.idRoom);
			var room = repo.createNewRoom(res.idRoom);
			var user = repo.addUserInRoom(res.fingerPrint, socket.id, true ,room)
			io.to(res.idRoom).emit('userJoinRoom', repo.displayUserConnected(res.idRoom));
		}
	});

    socket.on('writting', function (res) {
   		io.to(res.idRoom).emit('getCode', res.content);
		var room = repo.getRoom(res.idRoom);
   		room.setContent(res.content);
	});

	socket.on('disconnect', function () {
      	var connection = repo.deleteConnection(socket.id);
      	if(connection != null){
      		io.to(connection.room.idRoom).emit('userJoinRoom', repo.displayUserConnected(connection.room.idRoom));
      	}	
  	});

});